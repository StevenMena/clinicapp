<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons', function(Blueprint $table)
		{
			$table->integer('idPerson', true);
			$table->string('numDocument', 20)->nullable()->unique('numDocument_UNIQUE');
			$table->char('nit', 17)->nullable()->unique('nit_UNIQUE');
			$table->string('names');
			$table->string('lastnames');
			$table->date('bornDate');
			$table->integer('nationality')->nullable();
			$table->char('gener', 1);
			$table->string('address');
			$table->char('telephone', 10)->nullable();
			$table->char('cellphone', 10);
			$table->integer('idCity')->nullable();
			$table->char('bloodType', 5)->nullable();
			$table->integer('userCreated');
			$table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('userUpdated')->nullable();
			$table->timestamp('updatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons');
	}

}
