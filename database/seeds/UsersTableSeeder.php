<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $adminUser= new App\User;
        $adminUser->name='Administrador';
        $adminUser->email='admin@clinicapp.com';
        $adminUser->password=bcrypt('Ladecima10');
        $adminUser->remember_token = str_random(10);
        $adminUser->created_at = \Carbon\Carbon::now();
        $adminUser->updated_at = \Carbon\Carbon::now();
        $adminUser->save();
    }
}
